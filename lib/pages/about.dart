import 'package:flutter/material.dart';
import 'package:secret_calculator/theme_changer.dart';

class IDCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Me'),
        centerTitle: true,
        backgroundColor: Colors.green[500],
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.brightness_3),
            onPressed: () {
              ThemeBuilder.of(context).changeTheme();
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage(
                  'assets/michaelsusanto-profile.png',
                ),
                radius: 60.0,
              ),
            ),
            Text(
              'Full Name',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              'Michael Susanto',
              style: TextStyle(
                letterSpacing: 1.0,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'Nickname',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              'Michael',
              style: TextStyle(
                letterSpacing: 1.0,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'Hobbies',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              '- Coding',
              style: TextStyle(
                letterSpacing: 1.0,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              '- Badminton',
              style: TextStyle(
                letterSpacing: 1.0,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'Contact me',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage(
                    'assets/fb-logo.png',
                  ),
                  backgroundColor: Colors.transparent,
                  radius: 10.0,
                ),
                SizedBox(width: 10.0),
                Text(
                  'michael.s.155',
                  style: TextStyle(
                    fontSize: 16.0,
                    letterSpacing: 1.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage(
                    'assets/github-logo.png',
                  ),
                  backgroundColor: Colors.transparent,
                  radius: 10.0,
                ),
                SizedBox(width: 10.0),
                Text(
                  'michaelsusanto81',
                  style: TextStyle(
                    fontSize: 16.0,
                    letterSpacing: 1.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage(
                    'assets/linkedin-logo.png',
                  ),
                  backgroundColor: Colors.transparent,
                  radius: 10.0,
                ),
                SizedBox(width: 10.0),
                Text(
                  'michaelsusanto81',
                  style: TextStyle(
                    fontSize: 16.0,
                    letterSpacing: 1.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
