import 'package:flutter/material.dart';
import 'package:secret_calculator/pages/about.dart';
import 'package:secret_calculator/pages/home.dart';
import 'package:secret_calculator/pages/login.dart';
import 'package:secret_calculator/theme_changer.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeBuilder(
      defaultBrightness: Brightness.light,
      builder: (context, _brightness) {
        return MaterialApp(
          initialRoute: '/',
          routes: {
            '/': (context) => Login(),
            '/calculator': (context) => CalculatorApp(),
            '/about': (context) => IDCard(),
          },
          theme: ThemeData(
            brightness: _brightness,
          ),
        );
      },
    );
  }
}


